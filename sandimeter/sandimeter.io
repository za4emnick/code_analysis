1. 95% of classes are under 100 lines.
2. 84% of methods are under 5 lines.
3. 99% of method calls accepted are less than 4 parameters.
4. 100% of controllers have one instance variable per action.

Classes with 100+ lines
  Class name               | Size  | Path                                                 
  ActionText::Content      | 120   | ./node_modules/actiontext/lib/action_text/content.rb 
  ActionText::ContentTest  | 110   | ./node_modules/actiontext/test/unit/content_test.rb  

Methods with 5+ lines
  Class name                                  | Method name                           | Size  | Path                                                                        
  CreateActionTextTables                      | change                                | 10    | ./db/migrate/20181213122009_create_action_text_tables.action_text.rb        
  ActionText::Attachable                      | from_node                             | 9     | ./node_modules/actiontext/lib/action_text/attachable.rb                     
  ActionText::Attribute                       | has_rich_text                         | 18    | ./node_modules/actiontext/lib/action_text/attribute.rb                      
  ActionText::Serialization                   | dump                                  | 8     | ./node_modules/actiontext/lib/action_text/serialization.rb                  
  ActionText::AttachmentGallery               | find_attachment_gallery_nodes         | 9     | ./node_modules/actiontext/lib/action_text/attachment_gallery.rb             
  ActionText::TrixAttachment                  | from_attributes                       | 10    | ./node_modules/actiontext/lib/action_text/trix_attachment.rb                
  ActionText::Attachables::ContentAttachment  | attachable_plain_text_representation  | 6     | ./node_modules/actiontext/lib/action_text/attachables/content_attachment.rb 
  ActionText::Content                         | initialize                            | 7     | ./node_modules/actiontext/lib/action_text/content.rb                        
  ActionText::Fragment                        | wrap                                  | 8     | ./node_modules/actiontext/lib/action_text/fragment.rb                       
  CreateActiveStorageTables                   | change                                | 22    | ./db/migrate/20181213122008_create_active_storage_tables.active_storage.rb  
  MessagesController                          | create                                | 7     | ./node_modules/actiontext/test/dummy/app/controllers/messages_controller.rb 
  ActionText::TagHelper                       | rich_text_area_tag                    | 13    | ./node_modules/actiontext/app/helpers/action_text/tag_helper.rb             
  ActionText::ContentHelper                   | render_action_text_content            | 18    | ./node_modules/actiontext/app/helpers/action_text/content_helper.rb         

Misindented methods
  Class name                  | Method name  | Path                                                   
  ActionText::AttachmentTest  | proxied      | ./node_modules/actiontext/test/unit/attachment_test.rb 

Method calls with 4+ arguments
  # of arguments  | Path                                                   
  6               | ./node_modules/actiontext/lib/action_text/attribute.rb
